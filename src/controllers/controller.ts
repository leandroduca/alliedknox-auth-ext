import { Request, Response } from 'express';
import { Service } from '../services/service';

export const authenticate = async (req: Request, res: Response) => {

    const service = new Service();

    return res.json({
        accessToken: await service.authenticate(req.body),
        companiesId : await service.getCompaniesId()
    });
}

export const checkAuth = async (req: Request, res: Response) => {

    return res.json({
        accessToken: req.headers['x-access-token']
    });
}