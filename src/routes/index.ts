import { Router } from 'express';
import { asyncMiddleware } from 'allied-kernel';
import * as adController from '../controllers/controller';
import * as cors from 'cors';
import { swaggerSetup, swaggerUi } from '../configs/swagger';
import { verifyJWT } from '../utils/jwtValidation';

const router = Router();

router.options('*', cors());

router.use('/', swaggerUi.serve);
router.get('/api-docs', swaggerSetup);

// External Authentication endpoints
router.post('/accesstoken', cors(), asyncMiddleware(adController.authenticate));

router.use('/checktoken', verifyJWT);
router.get('/checktoken', cors(), asyncMiddleware(adController.checkAuth));

export default router;