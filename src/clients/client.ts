import { HttpClient } from 'allied-kernel';
import { getADErrorMessage, getADStatusCode } from '../utils/ad-error-utils';
import { ADIntegrationError } from '../errors/ad-integration-error';

class Client {
    
    private httpClient  = new HttpClient({baseURL: process.env.BASE_URL_AD});

    public constructor() {}

    public authenticate = async (postData) => {
        return await this.httpClient
            .post('', postData, { headers: { 'Accept': 'application/x-www-form-urlencoded' } })
            .catch((err) => { throw new ADIntegrationError(getADErrorMessage(err), getADStatusCode(err)); });
    }
}

export { Client }