import { KnexSingleton } from '../utils/knexSingleton';
import Reseller from '../interfaces/reseller';

class ClientDb {

    private conn = KnexSingleton.getInstance(process.env.DBHOST, process.env.DBUSER, process.env.DBPASSWORD, process.env.DBNAME).conn;

    public constructor() {}

    public getUserByName = async (name) => {

        return await this.conn
            .select('name', 'password', 'secret', 'id')
            .from('knoxUser')
            .where('name', name)
            .andWhere('isActive', true);
    }

    public async getCompaniesIdByUserId(userId: number): Promise<Reseller[]> {

        return this.conn
            .select('linxResellers.idempresa as id')
            .from('userReseller')
            .innerJoin('knoxUser', 'userReseller.idUser', 'knoxUser.id')
            .innerJoin('linxResellers', 'userReseller.idReseller', 'linxResellers.idempresa')
            .where('userReseller.idUser', userId);
    }

    public async getPermissionsByUserId(userId: number): Promise<any> {

        return this.conn
            .select(this.conn.raw('permissions.*, profilePermission.*'))
            .whereRaw('groups.id = userGroup.idGroup')
            .andWhereRaw('knoxUser.id = userGroup.idUser')
            .andWhereRaw('groups.id = profileGroup.idGroup')
            .andWhereRaw('profiles.id = profileGroup.idProfile')
            .andWhereRaw('permissions.id = profilePermission.idPermission')
            .andWhereRaw('profiles.id = profilePermission.idProfile')
            .from(this.conn.raw('knoxUser, groups, userGroup, profiles, profileGroup, permissions, profilePermission'))
            .where('knoxUser.id', userId);
    }
}

export { ClientDb }