import { Err } from 'allied-kernel';

class ADIntegrationError extends Err {

    public constructor(message: string, statusCode: number) {

        super(message, statusCode, 'ad-integration-error');
    }

} 

export { ADIntegrationError };