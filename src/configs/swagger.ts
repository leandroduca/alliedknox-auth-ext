import * as swaggerUi from 'swagger-ui-express';
import * as swaggerDocument from '../swagger.json';

export const swaggerSetup = swaggerUi.setup(swaggerDocument);
export { swaggerUi };