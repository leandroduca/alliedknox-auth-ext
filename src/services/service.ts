import { ADIntegrationError } from '../errors/ad-integration-error';
import { Client } from '../clients/client'
import { ClientDb } from '../clients/clientDb';
import * as jwt from 'jsonwebtoken';
import * as bcrypt from 'bcryptjs';
import { isNumber } from 'util';
import { AuthError } from '../errors/auth-error';
import Reseller from '../interfaces/reseller';

class Service {

    private clientDb: ClientDb;
    private userId: number;

    public constructor() {

        this.clientDb = new ClientDb();
    }

    public async authenticate(data) {
        await console.log(`adService.${this.authenticate.name}`);

        let user = await this.clientDb.getUserByName(data.user);

        if(user.length == 0) 
            throw new AuthError("Usuário inválido.", 400);
        
        if(!bcrypt.compareSync(data.password, user[0].password))
            throw new AuthError("Senha inválida.", 400);

        let validityToken: number = isNumber(data.validityToken) ? data.validityToken : 30;
        if (validityToken > 30 || validityToken < 1) //validityToken = 30;
            throw new ADIntegrationError('validityToken inválido.', 400);
        
        this.userId = user[0].id;

        let permissions = await this.clientDb.getPermissionsByUserId(this.userId);
        let accessToken = jwt.sign({ username: user[0].name, permissions: permissions }, process.env.JWT_SECRET, {
            expiresIn: validityToken + 'm' //token expires in minutes
        });


        return accessToken;
    } 

    public async getCompaniesId(): Promise<number[]> {

        let companiesId = await this.clientDb.getCompaniesIdByUserId(this.userId);
        
        return companiesId.map((elem): number => elem.id);
    }
}

export { Service };