import { IObjectTemplate } from 'allied-kernel';

const getADErrorMessage = (error: Error | IObjectTemplate | any): string => {

    if (error.response) {

        if (error.response.status == 502) {

            return 'AD Indisponível no momento.'
        }

        if (typeof error.response.data === 'string') {

            return error.response.data;
        }

        return error.response.data.error_description ? error.response.data.error_description : error.response.data.error;
    }

    return error.message;
}

const getADStatusCode = (error: any): number => {

    return error.response ? error.response.status : 502;
}

export { getADErrorMessage, getADStatusCode };