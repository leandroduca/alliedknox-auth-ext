import { isNullOrUndefined } from "util";
import { ADIntegrationError } from "../errors/ad-integration-error";

class AdRequest {
    
    public username: string;
    public password: string;

    constructor(data) {
        this.username = data.user;
        this.password = data.password;

        this.validate();
    }

    private validate = () => {
        if (isNullOrUndefined(this.username)) 
            throw new ADIntegrationError('Usuário necessário.', 401);

        if (isNullOrUndefined(this.password)) 
            throw new ADIntegrationError('Senha necessária.', 401);
    }
}

export default AdRequest;